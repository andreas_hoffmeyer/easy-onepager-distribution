<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Easy Onepager',
    'description' => 'TYPO3 distribution providing a onepage layout based on TypoScript, Backendlayouts, Fluid, RealURL and Gridelements',
    'author' => 'Andreas Hoffmeyer',
    'author_email' => 'hallo@andreas-hoffmeyer.de',
    'author_company' => '',
    'version' => '0.0.1',
    'state' => 'alpha',
    'category' => 'distribution',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-7.6.99',
            'gridelements' => '7.0.0-8.99.99',
            'realurl' => '2.0.0-2.0.99'
        ]
    ]
];