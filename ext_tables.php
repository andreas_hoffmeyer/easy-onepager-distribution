<?php

if (!defined("TYPO3_MODE")) {
    die("Access denied");
}

$boot = function($extKey) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extKey,
        'Configuration/TypoScript',
        'Easy Onepager'
    );
};

$boot($_EXTKEY);

unset($boot);