<?php

if (!defined('TYPO3_MODE')) {
    die("Access denied");
}

$boot = function($extKey) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    <INCLUDE_TYPOSCRIPT: source="FILE: EXT:easy_onepager/Configuration/TSConfig/Page.ts">
    ');
};

$boot($_EXTKEY);

unset($boot);