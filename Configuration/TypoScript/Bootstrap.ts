// Page configuration
page = PAGE
page {

    config {
        headerComment = Easy Onepager - by Andreas Hoffmeyer <hallo@andreas-hoffmeyer.de>

        // remove unused code
        removeDefaultJS = 1
        removeDefaultCss = 1

        // no comments polluting the markup
        disablePrefixComment = 1

        // URL settings
        tx_realurl_enable = 1
        absRefPrefix = /
    }

    // meta tags
    meta {
        author.field = author
        description.field = description
        keywords.field = keywords
    }

    // CSS includes
    includeCSS {
        fontawesome = {$plugin.tx_easyonepager.fontawesomeCSS}
        fontawesome.external = 1

        bootstrap = {$plugin.tx_easyonepager.bootstrapCSS}
        bootstrap.external = 1

        custom = {$plugin.tx_easyonepager.customCssFile}
    }

    // include JS libs in Footer like Bootstrap, jQuery ...
    includeJSFooterlibs {
        jQuery = https://code.jquery.com/jquery-2.2.4.min.js
        jQuery.external = 1
        jQuery.forceOnTop = 1

        smoothScroll = https://cdnjs.cloudflare.com/ajax/libs/jquery-smooth-scroll/2.0.0/jquery.smooth-scroll.min.js
        smoothScroll.external = 1
        // don't load this JS on other pages but homepage
        smoothScroll.if.value = {$plugin.tx_easyonepager.onepageStart}
        smoothScroll.if.equals.data = page:uid
    }

    // include JS in Footer
    includeJSFooter {
        bootstrap = {$plugin.tx_easyonepager.bootstrapJS}
        bootstrap.external = 1

        custom = {$plugin.tx_easyonepager.customJsFile}
        // don't load this JS on other pages but homepage
        custom.if.value = {$plugin.tx_easyonepager.onepageStart}
        custom.if.equals.data = page:uid
    }

    10 = FLUIDTEMPLATE
    10 {
        // Default layout definition
        file = EXT:easy_onepager/Resources/Private/Templates/EasyOnepager.html
        layoutRootPath = {$plugin.tx_easyonepager.layoutRootPath}
        partialRootPath = {$plugin.tx_easyonepager.partialRootPath}

        variables {
            // some nice little header
            homepageHeader < HomepageHeader

            // if there is some content on the homepage, render it here
            homepageContent < HomepageContent

            // render the onepage content
            onepageContent < OnepageContent
        }
    }
}

// Adjustments of content types
// If there are files in the defined directories, those will be used
lib.fluidContent {
    templateRootPaths {
        99 = EXT:easy_onepager/Resources/Private/Templates/Content/Templates/
    }
    layoutRootPaths {
        99 = EXT:easy_onepager/Resources/Private/Templates/Content/Layouts/
    }
    partialRootPaths {
        99 = EXT:easy_onepager/Resources/Private/Templates/Content/Partials/
    }
}