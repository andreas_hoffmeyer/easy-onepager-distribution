prototype.TopNavi = HMENU
prototype.TopNavi {
    1 = TMENU
    1 {
        wrap = <ul class="nav navbar-nav">|</ul>

        NO = 1
        NO {
            linkWrap = <li>|</li>
        }

        // Though as onepager we don't need this instantly on level 1,
        // we maybe want to inherit this later on
        ACT < .NO
        ACT {
            linkWrap = <li class="active">|</li>
        }

        CUR < .ACT
    }
}

TopNavi < prototype.TopNavi