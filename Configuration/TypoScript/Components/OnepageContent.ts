// Prototype for the CONTENT object to get content from tt_cntent table
prototype.GetContent = CONTENT
prototype.GetContent {
    table = tt_content
    select.andWhere = colPos=0
    select.pidInList.data = field:uid
    renderObj < tt_content
}

// prototype of default Backendlayout
// because in my eyes it's never a good idea to inherit from a sibling
prototype.DefaultBackendlayout = FLUIDTEMPLATE
prototype.DefaultBackendlayout {
    // the partials and default layouts should be inherited from the page itself
    layoutRootPath = {$plugin.tx_easyonepager.layoutRootPath}
    partialRootPath = {$plugin.tx_easyonepager.partialRootPath}
}

// prototype of a REGISTER to store the section id with /# replacement
prototype.SectionId = LOAD_REGISTER
prototype.SectionId {
    sectionId.cObject = TEXT
    sectionId.cObject.data = field:url
    sectionId.cObject {
        replacement {
            10 {
                search = /#
                replace =
            }
        }
    }
}

// prototype of CASE object to switch Backendlayouts
prototype.SwitchBeLayouts = CASE
prototype.SwitchBeLayouts {
    stdWrap.innerWrap.cObject < prototype.SectionId
    stdWrap.wrap = <section id="{register:sectionId}">|</section>
    stdWrap.insertData = 1
    // there is no need for using backendlayout on next level, because we're dealing with an easy onepager
    // so there is none
    key.field = backend_layout
    // defining the default first, to get sure that we definitely have one
    default < prototype.DefaultBackendlayout
    default {
        file = EXT:easy_onepager/Resources/Private/Templates/EasyOnepagerDefaultTmpl.html

        variables {
            content < prototype.GetContent
        }
    }

    // Backendlayout with two cols (66 / 33)
    pagets__twoColumns < prototype.DefaultBackendlayout
    pagets__twoColumns {
        file = EXT:easy_onepager/Resources/Private/Templates/EasyOnepagerTwoColumnsTmpl.html
        variables {
            leftContent < prototype.GetContent
            rightContent < prototype.GetContent
            rightContent.select.andWhere = colPos=1
        }
    }

    // Backendlayout with two cols (33 / 33 / 33)
    pagets__threeColumns < prototype.DefaultBackendlayout
    pagets__threeColumns {
        file = EXT:easy_onepager/Resources/Private/Templates/EasyOnepagerThreeColumnsTmpl.html
        variables {
            leftContent < prototype.GetContent

            centerContent < prototype.GetContent
            centerContent.select.andWhere = colPos=1

            rightContent < prototype.GetContent
            rightContent.select.andWhere = colPos=2
        }
    }
}

// prototype of the Onepage content to get all together
// to get the connection to the topmenu you should use the HMENU object
// ++++
// As HMENU and TMENU are common constructs we keep them together
// Another solution could be to define the topnavi and the onepage content section with the same prototype
prototype.OnepageContent = HMENU
prototype.OnepageContent {
    // define where to start from for the onepage contents
    special = directory
    special.value = {$plugin.tx_easyonepager.onepageStart}
    // from this point on you can also declare subsection by defining an IFSUB
    1 = TMENU
    1 {
        NO = 1
        NO {
            // as we don't want any kind of links or something, disable the default behaviour
            doNotLinkIt = 1
            stdWrap.cObject < prototype.SwitchBeLayouts
        }
    }
}

OnepageContent < prototype.OnepageContent