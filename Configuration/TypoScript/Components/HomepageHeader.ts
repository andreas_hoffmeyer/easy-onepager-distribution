prototype.ImageDetails = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
prototype.ImageDetails {
    references {
        table = pages
        fieldName = media
        uid = {$plugin.tx_easyonepager.onepageStart}
    }
}

prototype.HomepageHeader = FLUIDTEMPLATE
prototype.HomepageHeader {
    // make sure that you're on the homepage
    stdWrap.if.value = {$plugin.tx_easyonepager.onepageStart}
    stdWrap.if.equals.data = page:uid
    file = EXT:easy_onepager/Resources/Private/Templates/HomepageHeader.html

    dataProcessing.10 < prototype.ImageDetails
    dataProcessing.10.as = HeaderImage
}

HomepageHeader < prototype.HomepageHeader