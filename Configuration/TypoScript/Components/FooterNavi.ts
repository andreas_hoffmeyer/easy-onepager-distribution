prototype.FooterNavi = HMENU
prototype.FooterNavi {
    special = directory
    special.value = {$plugin.tx_easyonepager.footerPid}

    1 = TMENU
    1 {
        NO = 1
        NO {
            linkWrap = ||*| &nbsp;&#124;&nbsp; |*|
        }
    }
}

FooterNavi < prototype.FooterNavi