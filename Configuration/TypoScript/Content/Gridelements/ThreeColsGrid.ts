// Configure Gridelements
// Should be rendered within Fluid
tt_content.gridelements_pi1.20.10.setup {
    threeColsGrid < lib.gridelements.defaultGridSetup
    threeColsGrid {
        cObject = FLUIDTEMPLATE
        cObject {
            file = EXT:easy_onepager/Resources/Private/Templates/Content/Gridelements/ThreeColsGrid.html
        }
    }
}