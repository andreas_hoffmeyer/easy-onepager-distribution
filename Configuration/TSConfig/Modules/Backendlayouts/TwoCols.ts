mod.web_layout.BackendLayouts {
    twoColumns {
        title = Two Colums (66 / 33)
        config {
            backend_layout {
                colCount = 3
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Left
                                colPos = 0
                                colspan = 2
                            }
                            2 {
                                name = Right
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }
    }
}