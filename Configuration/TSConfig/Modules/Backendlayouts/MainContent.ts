// backendlayout will act as default as long as you don't configure it otherwise in your TS
mod.web_layout.BackendLayouts {
    mainContent {
        title = Main Content
        config {
            backend_layout {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = Main Content
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }
}

