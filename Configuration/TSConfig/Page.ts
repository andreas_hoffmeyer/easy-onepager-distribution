<INCLUDE_TYPOSCRIPT: source="DIR: EXT:easy_onepager/Configuration/TSConfig/Modules/Backendlayouts/" extensions="ts">
<INCLUDE_TYPOSCRIPT: source="DIR: EXT:easy_onepager/Configuration/TSConfig/Modules/Gridelements/" extensions="ts">

// Manipulating form elements for tt_content
TCEFORM.tt_content {
    layout {
        // rename layout 1 for a better understanding what it does
        altLabels {
            1 = Rounded images and centered text
        }
        // remove all unused fields
        removeItems = 2, 3
    }
}