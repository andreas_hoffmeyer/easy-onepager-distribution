/**
    Main JS file.
    Depends on jQuery

    @author Andreas Hoffmeyer <hallo@andreas-hoffmeyer.de>
 */
(function($, window) {
    // some code here
    $('#navbar a').on('click', function() {
        var target = $(this).attr('href');
        // because we have a preventDefault on the nav links
        // we need to push the href target to the browser to be able to bookmark the sectsions
        window.history.pushState(null, "", target);
    });
    // smooth scrolling using jQuery plugin
    // https://github.com/kswedberg/jquery-smooth-scroll
    $('#navbar a, .claim a.btn, a.navbar-brand').smoothScroll({
        offset: -50
    });
})(jQuery, window);
